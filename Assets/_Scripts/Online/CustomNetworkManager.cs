using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;
using Steamworks;

public class CustomNetworkManager : NetworkManager
{
    [SerializeField]private PlayerObjectController GamePlayerPrefab;
    [SerializeField] private PlayerObjectController AssassinPlayerPrefab;
    public List<PlayerObjectController> GamePlayers { get; } = new List<PlayerObjectController>();

    public int randomSelection;
    public int selectionCount = 0;
    public bool assassinCheck = true;
    


    private void Start()
    {
        randomSelection = Random.Range(0, 4);    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        if (selectionCount <= 4)
        {
            if (randomSelection == selectionCount)
            {
                assassinCheck = false;
                if (assassinCheck == false)
                {
                    if (SceneManager.GetActiveScene().name == "Lobby")
                    {
                        PlayerObjectController GamePlayerInstance = Instantiate(AssassinPlayerPrefab);
                        GamePlayerInstance.ConnectionID = conn.connectionId;
                        GamePlayerInstance.PlayerIdNumber = GamePlayers.Count + 1;
                        GamePlayerInstance.PlayerSteamID = (ulong)SteamMatchmaking.GetLobbyMemberByIndex((CSteamID)SteamLobby.Instance.CurrentLobbyID, GamePlayers.Count);
                        
                        assassinCheck = true;

                        NetworkServer.AddPlayerForConnection(conn, GamePlayerInstance.gameObject);

                    }
                }
            }
            if (randomSelection < selectionCount || randomSelection > selectionCount && assassinCheck == true)
            {
                if (SceneManager.GetActiveScene().name == "Lobby")
                {
                    PlayerObjectController GamePlayerInstance = Instantiate(GamePlayerPrefab);
                    GamePlayerInstance.ConnectionID = conn.connectionId;
                    GamePlayerInstance.PlayerIdNumber = GamePlayers.Count + 1;
                    GamePlayerInstance.PlayerSteamID = (ulong)SteamMatchmaking.GetLobbyMemberByIndex((CSteamID)SteamLobby.Instance.CurrentLobbyID, GamePlayers.Count);
                    

                    NetworkServer.AddPlayerForConnection(conn, GamePlayerInstance.gameObject);
                }
            }
            selectionCount++;
        }
        
    }

    public void StartGame(string SceneName)
    {
        ServerChangeScene(SceneName);
    }
}
