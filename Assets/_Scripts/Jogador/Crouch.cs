using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class Crouch : MonoBehaviour
    {
        public Transform crouchPos;
        public Transform crouchOriginalPos;
        public Camera cam;
        public CharacterController con;
        public Transform capsule;
        public float conHeight = 1;
        public float CapsuleHeight = .5f;
        public FirstPersonController fpscon;
        public float range = 2f;


        void Start()
        {

        }


        void Update()
        {
            RaycastHit hit;
            bool raycastThing = Physics.Raycast(transform.position, Vector3.up, out hit, 0.5f);


            if (Input.GetKey(KeyCode.LeftControl) && raycastThing == false) 
            {
                cam.transform.position = crouchPos.transform.position;
                con.height = conHeight;
                capsule.localScale = new Vector3(1, CapsuleHeight, 1);
                fpscon.m_RunSpeed = 2f;
                fpscon.m_WalkSpeed = 2f;
            }

            else if(raycastThing == true)
            {
                cam.transform.position = crouchPos.transform.position;
                con.height = conHeight;
                capsule.localScale = new Vector3(1, CapsuleHeight, 1);
                fpscon.m_RunSpeed = 2f;
                fpscon.m_WalkSpeed = 2f;
            }
            else
            {
                cam.transform.position = crouchOriginalPos.transform.position;
                con.height = 2f;
                capsule.localScale = new Vector3(1, 1, 1);
                fpscon.m_RunSpeed = 10f;
                fpscon.m_WalkSpeed = 5f;
            }
        }
    }
}