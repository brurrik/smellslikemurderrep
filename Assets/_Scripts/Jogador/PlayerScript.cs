using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QuickStart
{
    public class PlayerScript : NetworkBehaviour
    {

       
        public float speed = 12f;

  
        public GameObject PlayerModel;

        public CharacterController controller;
        public Transform groundCheck;
        public float groundDistance = 0.5f;
        public LayerMask groundMask;

        Vector3 velocity;
        bool isGrounded;
        public float gravity = -9.81f;

        private void Start()
        {
            PlayerModel.SetActive(false);
            SetPosition();

        }
      

        void Update()
        {

            if (SceneManager.GetActiveScene().name == "Mapa_Hosp�cio")
            {
                if(PlayerModel.activeSelf == false)
                {
                    PlayerModel.SetActive(true);
                    Cursor.lockState = CursorLockMode.Locked;

                }
                if (hasAuthority)
                {
                    isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

                    if(isGrounded && velocity.y < 0)
                    {
                        velocity.y = -2f;
                    }

                    float x = Input.GetAxis("Horizontal");
                    float z = Input.GetAxis("Vertical");
                    

                    

                    Vector3 move = transform.right * x + transform.forward * z;
                    controller.Move(move*speed*Time.deltaTime);

                    velocity.y += gravity * Time.deltaTime;
                    controller.Move(velocity * Time.deltaTime);

                }
            }

        }

        public void SetPosition()
        {
            transform.position = new Vector3(Random.Range(620, 621), 28.48f, Random.Range(346, 347));
        }
    }
}
