using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PickKey : MonoBehaviour
{
    public Component doorcolliderhere;
    public GameObject keypicked;
    public GameObject InstructionPick;

    void Start()
    {
        InstructionPick.SetActive(false);
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.transform.tag == "Player")
        {
            InstructionPick.SetActive(true);
        }


        if (Input.GetKey(KeyCode.E))
        {
            doorcolliderhere.GetComponent<BoxCollider>().enabled = true;
            keypicked.GetComponent<BoxCollider>().enabled = false;
            keypicked.SetActive(false);
            InstructionPick.SetActive(false);
        }
          
    }

    private void OnTriggerExit(Collider collision)
    {
        InstructionPick.SetActive(false);
    }
}
