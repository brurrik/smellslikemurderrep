using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedDoor : MonoBehaviour
{
    public Component doorclosedcolliderhere;
    public GameObject keypicked;
    public GameObject InstructionDoorClosed;

    void Start()
    {
        InstructionDoorClosed.SetActive(false);
    }

    private void OnTriggerStay(Collider collision)
    {
        if (keypicked.activeSelf == false) {
            InstructionDoorClosed.SetActive(false);
        }

        if (collision.transform.tag == "Player" && keypicked.activeSelf)
        {
            InstructionDoorClosed.SetActive(true);
        }

        if (Input.GetKey(KeyCode.E))
        {
            doorclosedcolliderhere.GetComponent<BoxCollider>().enabled = true;
            InstructionDoorClosed.SetActive(false);
        }

    }

    private void OnTriggerExit(Collider collision)
    {
        InstructionDoorClosed.SetActive(false);
    }
}
