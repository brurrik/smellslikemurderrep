using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Interruptor : MonoBehaviour
{

    public Transform jogador;
    public AudioClip somCaixaLuzes;
    public KeyCode teclaAcenderLuz = KeyCode.E;
    [Range(1, 5)]
    public float distanciaMinima = 2;
    public bool luzLigada = true;
    [Space(15)]
    public GameObject objInterruptorOn;
    public GameObject objInterruptorOff;
    [Space(15)]
    float distancia;
    AudioSource audio;

    void Awake()
    {
        audio = GetComponent<AudioSource>();
        if (somCaixaLuzes)
        {
            audio.clip = somCaixaLuzes;
        }
        audio.clip = somCaixaLuzes;
        audio.playOnAwake = false;
        audio.loop = false;
        //

        if (objInterruptorOn)
        {
            objInterruptorOn.SetActive(luzLigada);

        }

        if (objInterruptorOff)
        {
            objInterruptorOff.SetActive(!luzLigada);

        }

        objInterruptorOn.SetActive(luzLigada);
        objInterruptorOff.SetActive(!luzLigada);
    }


    void Update()
    {
        if (jogador)
        {
            distancia = Vector3.Distance(transform.position, jogador.transform.position);
            if (distancia < distanciaMinima)
            {
                if (Input.GetKeyDown(teclaAcenderLuz))
                {
                    luzLigada = !luzLigada;
                    if (audio.clip != null)
                    {
                        audio.PlayOneShot(audio.clip);
                    }
                    //


                    if (objInterruptorOn)
                    {
                        objInterruptorOn.SetActive(luzLigada);

                    }

                    if (objInterruptorOff)
                    {
                        objInterruptorOff.SetActive(!luzLigada);

                    }

                    GameObject[] todasluzes = Resources.FindObjectsOfTypeAll<GameObject>().Where(g => g.CompareTag("luz")).ToArray(); //GameObject.FindGameObjectsWithTag("luz");
                    foreach (var item in todasluzes)
                    {
                        item.SetActive(luzLigada);
                    }

                    GameObject[] todasluzesdesligadas = Resources.FindObjectsOfTypeAll<GameObject>().Where(g => g.CompareTag("luz")).ToArray(); //GameObject.FindGameObjectsWithTag("luzdesligada");
                    foreach (var item in todasluzesdesligadas)
                    {
                        item.SetActive(!luzLigada);
                    }
                }
            }
        }
    }
}
