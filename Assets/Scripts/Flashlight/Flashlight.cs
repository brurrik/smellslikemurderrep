using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    //public Component flashlightcolliderhere;
   // public GameObject flashlightpicked;
    public GameObject InstructionFlash;
    public GameObject flashinfps;
    public GameObject flashlightLight;
    private bool flashlighton;

    void Start()
    {
        InstructionFlash.SetActive(false);
        flashlightLight.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && flashinfps.activeSelf)
        {
            if (flashlighton == false)
            {
                flashlightLight.gameObject.SetActive(true);
                flashlighton = true;
            }
            else
            {
                flashlightLight.gameObject.SetActive(false);
                flashlighton = false;
            }
        }

        if (Input.GetKey(KeyCode.E))
        {
            Pickup();
        }    
    }

    //private void OnTriggerStay(Collider collision)
    //{
    //    if (collision.transform.tag == "Player")
    //    {
    //        InstructionFlash.SetActive(true);
    //        flashinfps.SetActive(false);
    //    }


    //    if (Input.GetKey(KeyCode.E))
    //    {
    //        var flashlightpicked = GameObject.FindGameObjectWithTag("lanterna");
    //        flashlightpicked.GetComponent<BoxCollider>().enabled = true;
    //        //flashlightpicked.GetComponent<BoxCollider>().enabled = false;
            
    //        flashlightpicked.SetActive(false);

    //        InstructionFlash.SetActive(false);
    //        // deixar ficar comentado este c�digo comentado ser� entretanto trocado pelo c�digo de invent�rio
    //        //flashinfps.SetActive(true);

    //        inventario.ativaColecionavel(flashinfps);
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    InstructionFlash.SetActive(false);
    //}

    void Pickup()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 2))
        {
            if (hit.collider.CompareTag("Player"))
            {
                var flashlightpicked = GameObject.FindGameObjectWithTag("lanterna");
                flashlightpicked.GetComponent<BoxCollider>().enabled = true;
                flashlightpicked.SetActive(false);

                InstructionFlash.SetActive(false);
                // deixar ficar comentado este c�digo comentado ser� entretanto trocado pelo c�digo de invent�rio
                //flashinfps.SetActive(true);

                inventario.ativaColecionavel(flashinfps);
            }
        }
    }
}