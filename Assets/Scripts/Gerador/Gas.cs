using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gas : MonoBehaviour
{
    public Component gascolliderhere;
    public GameObject gaspicked;
    public GameObject InstructionGas;
    public GameObject gasinfps;
    private bool havegas;

    void Start()
    {
        InstructionGas.SetActive(false);
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.transform.tag == "Player")
        {
            InstructionGas.SetActive(true);
            gasinfps.SetActive(false);
        }


        if (Input.GetKey(KeyCode.E))
        {
            gascolliderhere.GetComponent<BoxCollider>().enabled = true;
            gaspicked.GetComponent<BoxCollider>().enabled = false;
            gaspicked.SetActive(false);
            InstructionGas.SetActive(false);
            gasinfps.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        InstructionGas.SetActive(false);
    }
}