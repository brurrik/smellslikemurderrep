using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressKeyCloseDoor : MonoBehaviour
{
    public GameObject InstructionOpen;
    public GameObject InstructionClose;
    public GameObject AnimeObject;
    public bool Action = false;

    public AudioSource closeDoor;

    void Start()
    {
        InstructionOpen.SetActive(false);
        InstructionClose.SetActive(false);
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.transform.tag == "Player" && GetComponent<PressKeyOpenDoor>().isClosed==false)
        {
            InstructionOpen.SetActive(false);
            InstructionClose.SetActive(true);
            Action = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        InstructionOpen.SetActive(false);
        InstructionClose.SetActive(false);
        Action = false;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && GetComponent<PressKeyOpenDoor>().isClosed == false)
        {
            if (Action == true)
            {
                
                AnimeObject.GetComponent<Animator>().Play("DoorClose");
                Action = false;
                GetComponent<PressKeyOpenDoor>().isClosed = true;
                closeDoor.Play();
            }
        }
    }
}

