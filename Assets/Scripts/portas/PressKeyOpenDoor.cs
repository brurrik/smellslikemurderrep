using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressKeyOpenDoor : MonoBehaviour
{
    public GameObject InstructionOpen;
    public GameObject InstructionClose;
    public GameObject AnimeObject;
    public bool Action = false;
    public bool isClosed = true;

    public AudioSource openDoor;

    void Start()
    {
        InstructionOpen.SetActive(false);
        InstructionClose.SetActive(false);
    }
    private void OnTriggerStay(Collider collision)
    {
        if(collision.transform.tag == "Player" && isClosed)
        {
            InstructionOpen.SetActive(true);
            InstructionClose.SetActive(false);
            Action = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        InstructionOpen.SetActive(false);
        InstructionClose.SetActive(false);
        Action = false;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isClosed)
        {
            if (Action == true)
            {
              
                AnimeObject.GetComponent<Animator>().Play("DoorOpen");
                Action = false;
                isClosed = false;
               openDoor.Play();
            }
        }
    }
}
