using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Vida : MonoBehaviour
{
    public static float vidaJogador = 100f;
    public static float vidaAtual = 0f;
    public TextMeshProUGUI vidaJogadortxt;
    public static bool fimJogo;


    private void Start()
    {
        vidaAtual = vidaJogador;
        fimJogo = false;
    }

    void Update()
    {
        vidaJogadortxt.text = vidaAtual.ToString();
    }

    public void indicaVida()
    {
       float estadoVida = vidaAtual / vidaJogador;
    }

    public static void tiraVida(int dano)
    {
        vidaAtual = vidaAtual - dano;
    }
}
