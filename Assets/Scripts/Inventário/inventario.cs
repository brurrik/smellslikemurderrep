using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class inventario : MonoBehaviour


{
    [SerializeField]
    public List<GameObject> colecionaveis = new List<GameObject>();

    public static Dictionary<GameObject, bool> colecionaveisAtivos = new Dictionary<GameObject, bool>();
    int equipado = 0;

    void Start()
    {
        foreach (var item in colecionaveis)
        {
            colecionaveisAtivos.Add(item, false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Mouse ScrollWheel") != 0f)
        {
            (colecionaveisAtivos.ToArray())[equipado].Key.SetActive(false);

            var x = Input.GetAxis("Mouse ScrollWheel");
            var colecionaveisColetados = colecionaveisAtivos.Where(c => c.Value == true);

            if (x > colecionaveisColetados.Count())
            {
                x = colecionaveisColetados.Count() - 1;
            }
            else if (x < 0)
            {
                x = 0;
            }

            equipado = x <= 0 ? 0 : (int)x - 1;

            (colecionaveisAtivos.ToArray())[equipado].Key.SetActive(true);
        }
    }

    public static void ativaColecionavel(GameObject colecionavel)
    {
        colecionaveisAtivos[colecionavel] = true;
    }
}
