using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armadilha : MonoBehaviour
{
    public float dano = 25f;
    public Transform jogador;
    public AudioClip somarmadi;
    [Range(1, 5)]
    public float distanciaMinima = 2;
    float distancia;
    public bool armadilhaAtiva = true;
    [Space(15)]
    public GameObject armaOn;
    public GameObject armaOff;

    AudioSource audio;

    void Awake()
    {
        audio = GetComponent<AudioSource>();
        if (somarmadi)
        {
            audio.clip = somarmadi;
        }
        audio.clip = somarmadi;
        audio.playOnAwake = false;
        audio.loop = false;

        //    //
        //    if (armaOn)
        //    {
        //        armaOn.SetActive(armadilhaAtiva);

        //    }

        //    if (armaOff)
        //    {
        //        armaOff.SetActive(!armadilhaAtiva);

        //    }


    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (jogador)
    //    {
    //        distancia = Vector3.Distance(transform.position, jogador.transform.position);
    //        if (distancia < distanciaMinima)
    //        {
    //            // if (Collision.gameobject.tag == "Player")
    //            {
    //                armadilhaAtiva = !armadilhaAtiva;

    //            }
    //        }
    //    }
    //}

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.tag == "Player" && armadilhaAtiva)
        {
            armadilhaAtiva = !armadilhaAtiva;
            armaOn.SetActive(!armadilhaAtiva);
            armaOff.SetActive(armadilhaAtiva);
            if (audio.clip != null)
            {
                audio.PlayOneShot(audio.clip);
            }
            Vida.tiraVida(10);
        }
    }
}