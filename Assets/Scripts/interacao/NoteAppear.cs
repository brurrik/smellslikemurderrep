using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteAppear : MonoBehaviour
{
    [SerializeField]
    private Image _noteImage;
    public Image _background;
    public Text Pista;
    public GameObject InstructionNote;
    public bool Action = false;
    bool apanhaNote = false;

    public AudioSource pickUpNote;

    void Start()
    {
        _background.enabled = false;
        InstructionNote.SetActive(false);
        Pista.enabled = false;
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.transform.tag == "Player" )
        {
            if (apanhaNote == false) 
            {
                InstructionNote.SetActive(true);
                Action = true;
                Pista.enabled = false;
            }
            else
            {
                InstructionNote.SetActive(false);
                Action = false;
                Pista.enabled = true;
            }
            
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        InstructionNote.SetActive(false);
        _noteImage.enabled = false;
        _background.enabled = false;
        Action = false;
        apanhaNote = false;
        Pista.enabled = false;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && apanhaNote == false)
        {
            if (Action == true)
            {
                _noteImage.enabled = true;
                _background.enabled = true;
                pickUpNote.Play();
                apanhaNote = true;
                Pista.enabled = true;
            }
        }
    }
}
